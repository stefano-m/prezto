#
# Enables battery information support
#
# Authors:
#   Stefano Mazzucco <stefano@curso.re>

# if no battery support, return 1
if (( ! $+commands[upower] && ! $+commands[acpi] && ! $+commands[ioreg] )); then
  return 1
fi
