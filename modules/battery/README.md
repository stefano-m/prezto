Battery
========

Expose information about the battery status.

Requires `upower` (faster) or `acpi`.

Theming
-------

To display the name of the current virtual enviroment in a prompt, define the
following style in the `prompt_name_setup` function.

    # %v - battery status.
    zstyle ':prezto:module:battery:info:battery_status' format 'battery status:%v'

Then add `battery_info[battery_status]` to `$PROMPT` or `$RPROMPT` and call
`battery-info` in the `prompt_name_precmd` hook function.

Authors
-------

[Stefano Mazzucco](https://notabug.org:stefano-m)