#
# Exposes information about the battery via the $battery_info
# associative array.
#
# Authors:
#   Stefano Mazzucco <stefano@curso.re>
#
function battery-info()
{
    setopt LOCAL_OPTIONS

    unset battery_info
    typeset -gA battery_info

    local LOW_BAT=10
    local MID_BAT=50

    local report
    local battery

    local info_format
    local -A info_formats

    local percent
    local percent_format
    local percent_formatted

    local arrow_up
    local arrow_up_format
    local arrow_up_formatted

    local arrow_down
    local arrow_down_format
    local arrow_down_formatted

    local red=0
    local red_format
    local red_formatted

    local yellow=0
    local yellow_format
    local yellow_formatted

    local green=0
    local green_format
    local green_formatted

    if (( $+commands[upower] )); then

        battery=/org/freedesktop/UPower/devices/DisplayDevice

        info="`upower --show-info "$battery"`"

        percent=`echo $info | grep -oe "percentage: \+[0-9]\{1,3\}" | sed 's/percentage: \+//'`

        report=`echo -e $info | grep -oe "state: \+\b[a-z\-]\{1,\}\b" | sed 's/state: \+//'`

    elif (( $+commands[acpi] )); then
        info=`acpi -b`

        percent=`echo $info | grep -oe "[0-9]\{1,3\}%" | sed 's/%//'`

        report=`echo $info | grep -oe "\(\(C\|Disc\)harging\)"`
    elif (( $+commands[ioreg] )); then

      info=`ioreg -n AppleSmartBattery -r`

      percent=`echo $info | awk '$1~/Capacity/{c[$1]=$3} END{OFMT="%.2f"; max=c["\"MaxCapacity\""]; print (max>0? 100*c["\"CurrentCapacity\""]/max: "?")}'`

      report=`echo $info | awk '$1~/ExternalConnected/{gsub("Yes", "charging");gsub("No", "discharging"); print $3}'`
    fi

    case $report in
        Charging|charging)
            zstyle -s ':prezto:module:battery:info:arrow_up' format \
                   'arrow_up_format'
            zformat -f arrow_up_formatted "$arrow_up_format" \
                    "u:$arrow_up"
            ;;
        Discharging|discharging)
            zstyle -s ':prezto:module:battery:info:arrow_down' format \
                   'arrow_down_format'
            zformat -f arrow_down_formatted "$arrow_down_format" \
                    "d:$arrow_down"
            ;;
    esac

    # Percent
    zstyle -s ':prezto:module:battery:info:percent' format 'percent_format'
    if [[ -n $percent ]]; then
        zformat -f percent_formatted "$percent_format" "p:$percent%%"

        # Color
        if [[ $percent -le $LOW_BAT ]]; then
            red=1
            yellow=0
            green=0
            zstyle -s ':prezto:module:battery:info:red' format \
                   'red_format'
            zformat -f red_formatted "$red_format" "r:$red"
        elif [[ $percent -le $MID_BAT ]]; then
            red=0
            yellow=1
            green=0
            zstyle -s ':prezto:module:battery:info:yellow' format \
                   'yellow_format'
            zformat -f yellow_formatted "$yellow_format" "y:$yellow"
        else
            red=0
            yellow=0
            green=1
            zstyle -s ':prezto:module:battery:info:green' format \
                   'green_format'
            zformat -f green_formatted "$green_format" "g:$green"
        fi

    fi

    # Format info.
    zstyle -a ':prezto:module:battery:info:keys' format 'info_formats'
    for info_format in ${(k)info_formats}; do
        zformat -f REPLY "$info_formats[$info_format]" \
                "r:$red_formatted" \
                "y:$yellow_formatted" \
                "g:$green_formatted" \
                "u:$arrow_up_formatted" \
                "d:$arrow_down_formatted" \
                "p:$percent_formatted"
        battery_info[$info_format]="$REPLY"
    done

    unset REPLY

    return 0
}


battery-info "$@"
